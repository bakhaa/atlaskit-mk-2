# @atlaskit/progress-bar

## 0.2.0
- [minor] [06e6dd5731](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/06e6dd5731):

  - Initial release of Progress Bar component.

## 0.1.0
- [minor] [b2eb85b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b2eb85b):

  - Initial release of Progress Bar component.

