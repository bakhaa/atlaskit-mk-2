import { sizeOpts } from './types';

export const sizes: { [key in sizeOpts]: string } = {
  small: '16px',
  medium: '24px',
  large: '32px',
  xlarge: '48px',
};
