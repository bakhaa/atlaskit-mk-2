import Icon, { sizeObject } from './components/Icon';

export default Icon;
export { sizeObject as size };
export { default as Skeleton } from './components/Skeleton';
export { default as metadata } from './metadata';
